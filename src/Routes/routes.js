import LoginPage from "pages/LoginPage/LoginPage";

export const routes = [
  {
    path: "/login",
    component: <LoginPage />,
  },
];
